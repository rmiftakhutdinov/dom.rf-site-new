package site;

public class Configuration {

    public static String os = "windows"; // operation system (windows, linux or macos)
    public static String browser = "chrome"; // browser (firefox or chrome)
    public static boolean closeDriverAfterTests = true; // true - close browser after tests, false - stay browser in open state

    static String devStage = "https://dev.stage.domrf.orbita.center/";
    static String testStage = "https://dev.domrf.cometrica.ru/";

    public static String url = testStage;

}
