package site.framework.helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.awt.*;
import java.awt.datatransfer.StringSelection;

public class BaseHelper {

    protected WebDriver driver;

    public BaseHelper(WebDriver driver){
        this.driver = driver;
    }

    public void sendKeys(By by, String string){

        findElement(by).clear();
        findElement(by).sendKeys(string); // Вставить текст

    }

    public void afterExceptionMessage(String message){

        throw new RuntimeException("\n" + message); // Сообщение об ошибке

    }

    public void sleep(int time) throws InterruptedException {

        Thread.sleep(time); // Таймаут по заданному времени

    }

    public void click(By by){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(by)).click();
        } catch (TimeoutException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице или не кликабелен");
        }

    }

    public void click(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(by)).click(); // Ожидание элемента и клик по нему

    }

    public void waitBy(By by, int time){

        new WebDriverWait(driver, time)
                .until(ExpectedConditions.visibilityOfElementLocated(by)); // Ожидание элемента с выбором времени

    }

    public void waitBy(By by){

        new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOfElementLocated(by)); // Ожидание элемента с установленным по дефолту временем

    }

    public WebElement findElement(By by){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(by)); // Поиск и ожидание элемента
        } catch (java.util.NoSuchElementException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице");
        }

        return driver.findElement(by);

    }

    public void scrollToElement(By by) throws InterruptedException {

        WebElement element = driver.findElement(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element); // Скролл к элементу
        sleep(1000);

    }

    public boolean isElementPresent(By selector){

        try{
            driver.findElement(selector); // Проверка видимости элемента на странице
            return true;
        } catch (NoSuchElementException ex){
            return false;
        }

    }

    public void isElementNotPresent(By selector){

        try{

            new WebDriverWait(driver, 3).until(ExpectedConditions.presenceOfElementLocated(selector)); // Ожидание отсутствия элемента на странице

        } catch (TimeoutException ignored){

        }

    }

    public void setClipboardData(String string) {

        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null); // Добавление файла

    }

    public void waitForElementIsInvisibility(By by){

        new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(by)); // Ожидание исчезновения элемента

    }

    // drug and drop in the search page
    private Integer getStep(WebElement SliderTrack) {

        return SliderTrack.getSize().width / 4;

    }

    private Integer getCurrentPosition(WebElement priceLow, WebElement sliderTrack) {

        Integer sliderCenterPx = Integer.parseInt(priceLow.getCssValue("left").replaceAll("px", "")) + priceLow.getSize().width / 2;
        return sliderCenterPx / getStep(sliderTrack) + 1;

    }

    public void setSliderPosition(Integer position, WebElement sliderTrack, WebElement priceLow) {

        Integer xOffset = (position - getCurrentPosition(priceLow, sliderTrack)) * getStep(sliderTrack);
        Actions actions = new Actions(driver);
        actions.dragAndDropBy(priceLow, xOffset, 0).perform();

    }

}
