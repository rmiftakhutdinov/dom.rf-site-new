package site.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.Test;
import site.BaseTest;
import site.Configuration;
import site.pageObjects.HomePage;
import site.pageObjects.LinerLandingPage;
import static java.lang.Thread.sleep;

public class InvestorsTest extends BaseTest {

    @Test(description = "Переход на лендинг 'Лайнер'")
    protected void goToLinerFromInvestorsPage() {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url + "for-investors");

        try {
            action().waitBy(page.investorsPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Инвесторам' не найден");
        }

        action().click(page.linerInvestors);

        new LinerLandingPage(driver).findLinerLandingPageElement();

    }

    @Test(description = "Переход на 'Перечеь компаний'", priority = 1)
    public void goToCompanyList() throws InterruptedException {

        HomePage page = new HomePage(driver);

        String handleBefore = driver.getWindowHandle();

        driver.get(Configuration.url + "for-investors");

        try {
            action().waitBy(page.investorsPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Инвесторам' не найден");
        }

        action().click(page.listOfCompanyInvestors);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id = "about/controls/";
        Assert.assertTrue(driver.getCurrentUrl().contains(id));

        try {
            action().waitBy(page.elementListOfCompanyInvestors);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Элемент страницы 'О компании' не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

    }

}
