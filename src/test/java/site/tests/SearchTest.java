package site.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import site.BaseTest;
import site.Configuration;
import site.pageObjects.AdminPanelPage;
import site.pageObjects.HomePage;
import site.pageObjects.LinerLandingPage;
import site.pageObjects.SearchPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static java.lang.Thread.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.testng.Assert.assertTrue;

public class SearchTest extends BaseTest {

    @Test(description = "Фильтрация по количеству комнат")
    public void roomsFilter() throws InterruptedException {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        action().click(page.roomStudio);

        action().waitBy(page.spinner);
        action().waitForElementIsInvisibility(page.spinner);

        try {
            Assert.assertTrue(action().findElement(page.roomStudio).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Радиобаттон 'Студия' не активный");
        }

        if (action().isElementPresent(new HomePage(driver).searchPageElement)){

            Set<String> elements = driver.findElements(By.xpath("//div[@class='aptm_search-item_header']/div[contains(text(),'Студия')]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

            for (String element : elements) {

                String[] roomStudio = element.split(",");
                Assert.assertTrue(roomStudio[0].contains("Студия"));
                Assert.assertFalse(roomStudio[0].contains("2 ком."));
                Assert.assertFalse(roomStudio[0].contains("3 ком."));

            }

            action().click(page.roomStudio);

            action().waitBy(page.spinner);
            action().waitForElementIsInvisibility(page.spinner);

        } else {

            action().click(page.resetFilters);

            action().waitBy(page.spinner);
            action().waitForElementIsInvisibility(page.spinner);

        }

        sleep(1000);

        try {
            Assert.assertFalse(action().findElement(page.roomStudio).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Радиобаттон 'Студия' активный");
        }

        action().click(page.room2);

        action().waitBy(page.spinner);
        action().waitForElementIsInvisibility(page.spinner);

        try {
            Assert.assertTrue(action().findElement(page.room2).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Радиобаттон '2' не активный");
        }

        if (action().isElementPresent(new HomePage(driver).searchPageElement)){

            Set<String> elements = driver.findElements(By.xpath("//div[@class='aptm_search-item_header']/div[contains(text(),'2 ком.')]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

            for (String element : elements) {

                String[] roomStudio = element.split(",");
                Assert.assertFalse(roomStudio[0].contains("Студия"));
                Assert.assertTrue(roomStudio[0].contains("2 ком."));
                Assert.assertFalse(roomStudio[0].contains("3 ком."));

            }

            action().click(page.room2);

            action().waitBy(page.spinner);
            action().waitForElementIsInvisibility(page.spinner);

        } else {

            action().click(page.resetFilters);

            action().waitBy(page.spinner);
            action().waitForElementIsInvisibility(page.spinner);

        }

        sleep(1000);

        try {
            Assert.assertFalse(action().findElement(page.room2).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Радиобаттон '2' активный");
        }

        action().click(page.room3);

        action().waitBy(page.spinner);
        action().waitForElementIsInvisibility(page.spinner);

        try {
            Assert.assertTrue(action().findElement(page.room3).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Радиобаттон '3' не активный");
        }

        if (action().isElementPresent(new HomePage(driver).searchPageElement)){

            Set<String> elements = driver.findElements(By.xpath("//div[@class='aptm_search-item_header']/div[contains(text(),'2 ком.')]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

            for (String element : elements) {

                String[] roomStudio = element.split(",");
                Assert.assertFalse(roomStudio[0].contains("Студия"));
                Assert.assertTrue(roomStudio[0].contains("2 ком."));
                Assert.assertFalse(roomStudio[0].contains("3 ком."));

            }

            action().click(page.room2);

            action().waitBy(page.spinner);
            action().waitForElementIsInvisibility(page.spinner);

        } else {

            action().click(page.resetFilters);

            action().waitBy(page.spinner);
            action().waitForElementIsInvisibility(page.spinner);

        }

        sleep(1000);

        try {
            Assert.assertFalse(action().findElement(page.room3).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Радиобаттон '3' активный");
        }

    }

    @Test(description = "Фильтрация по количеству этажей", priority = 1)
    public void floorsFilter() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        action().sendKeys(page.floorFrom, "12");
        action().sendKeys(page.floorTo, "15");

        try {
            Assert.assertFalse(action().findElement(page.floor15).getText().contains("эт. 10/15"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Найден элемент 10-го этажа");
        }

        action().sendKeys(page.floorFrom, "15");

        action().click(page.notEndFloor);

        try {
            action().findElement(page.resetFilters).isDisplayed();
        } catch (RuntimeException ex){
            action().afterExceptionMessage("Элемент кнопки сброса фильтров не найден");
        }

    }

    @Test(description = "Фильтрация по цене", priority = 2)
    public void priceFilter() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        WebElement priceLowStart = action().findElement(page.priceLow);
        priceLowStart.getAttribute("style");

        WebElement priceHigh = action().findElement(page.priceHigh);
        priceHigh.getAttribute("style");

        WebElement sliderTrack = action().findElement(page.sliderTrack);

        action().setSliderPosition(2, sliderTrack, priceLowStart);

        WebElement priceLowFinish = action().findElement(page.priceLow);
        priceLowFinish.getAttribute("style");

        Assert.assertFalse(priceLowStart == priceLowFinish);

        action().setSliderPosition(3, sliderTrack, priceHigh);

        WebElement priceHighFinish = action().findElement(page.priceHigh);
        priceHighFinish.getAttribute("style");

        Assert.assertFalse(priceLowStart == priceLowFinish);

    }

    @Test(description = "Сброс фильтров", priority = 3)
    public void resetFilters() throws InterruptedException {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        action().findElement(page.floorFrom).sendKeys("55");
        action().findElement(page.floorTo).sendKeys("99");

        try {
            action().waitBy(page.resetFilters, 5);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки сброса фильтров не найден");
        }

        sleep(1000);

        action().click(page.resetFilters);

        page.findSearchPageElement();

    }

    @Test(description = "Свернуть и развернуть фильтр", priority = 4)
    public void hideAndShowFilter() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        action().findElement(page.apartment).sendKeys(Keys.PAGE_DOWN);

        try {
            action().waitBy(page.showFilter, 5);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент свернутого фильтра не найден");
        }

        action().click(page.showFilter);

        try {
            action().waitBy(page.notEndFloor, 5);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого фильтра не найден");
        }

    }

    @Test(description = "Переход в карточку апартамента", priority = 5)
    public void goToApartment() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        action().click(page.apartment);

        page.findApartmentElement();

    }

    @Test(description = "Переход на 'Лайнер' по кнопке 'Показать'", priority = 6)
    public void goToLinerFromSearchPage() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        action().click(page.goToLiner);

        new LinerLandingPage(driver).findLinerLandingPageElement();

    }

    @Test(description = "Пагинация", priority = 7)
    public void pagination() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        if (action().isElementPresent(page.page2)){

            action().click(page.page2);

            try {
                action().findElement(page.page2Active).isDisplayed();
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент страницы 2 не найден");
            }

        } else {
            System.out.println("Элемент страницы 2 не найден");
        }

        if (action().isElementPresent(page.page1)){

            action().click(page.page1);

            try {
                action().findElement(page.page1Active).isDisplayed();
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент страницы 1 не найден");
            }

        }

        if (action().isElementPresent(page.pageNext)){

            action().click(page.pageNext);

            try {
                action().findElement(page.page2Active).isDisplayed();
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент страницы 2 не найден");
            }

        }

    }

    @Test(description = "Отправка заявки при отсутствии свободных апартаментов", priority = 12)
    public void sendOrderFromSearchPage() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        try {
            action().waitBy(page.noApartmentsElement, 10);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы без свободных апартаментов не найден либо имеются свободные апартаменты");
        }

        action().sendKeys(page.inputName, "Тест Страница Поиска");
        action().sendKeys(page.inputPhone, "1112223344");

        List<WebElement> labels = driver.findElements(By.xpath("//label/span"));
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        action().click(page.sendOrder);

        try {
            action().waitBy(page.sendOrderElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент экрана успешной отправки заявки не найден");
        }

    }

    @Test(description = "Переход на лендинг 'Лайнер'", priority = 10)
    public void goToLinerFromApartmentPage() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        page.goToApartment();

        action().click(page.liner);

        new LinerLandingPage(driver).findLinerLandingPageElement();

    }

    @Test(description = "Прокликать фотогалерею", priority = 11)
    public void galleryPhoto() throws InterruptedException {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        page.goToApartment();

        try {
            action().waitBy(page.nextControl);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент переключения фото не найден");
        }

        String image1 = action().findElement(By.xpath("//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControl);
        sleep(500);

        String image2 = action().findElement(By.xpath("//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControl);
        sleep(500);

        String image3 = action().findElement(By.xpath("//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.prevControl);
        sleep(500);

        assertThat(image1.equals(image2) && image2.equals(image3) && image1.equals(image3), is(false));

        String img1 = action().findElement(By.xpath("//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        Actions actions = new Actions(driver);
        actions.moveToElement(action().findElement(page.gallery)).build().perform();

        try {
            action().waitBy(page.miniPhotos);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент миниатюры фотографии не найден");
        }

        action().click(page.miniPhotos);
        sleep(500);

        String img2 = action().findElement(By.xpath("//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        assertThat(img1.equals(img2), is(false));

        action().click(page.gallery);
        sleep(1000);

        String miniImg2 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        assertThat(img2.equals(miniImg2), is(true));

        action().click(page.prevControl);
        sleep(1000);

        String miniImg1 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        assertThat(miniImg1.equals(miniImg2), is(false));

        action().click(page.closeModalPhotos);

        try {
            action().isElementNotPresent(page.closeModalPhotos);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Модальное окно не закрылось");
        }

    }

    @Test(description = "Забронировать апартамент", priority = 8)
    public void reserveApart() throws InterruptedException, AWTException {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        page.goToApartment();

        action().click(page.reserve);

        try {
            action().waitBy(page.inputClientFirstName);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент окна 1-го шага в окне бронирования не найден");
        }

        action().sendKeys(page.inputClientFirstName, "Тест Карточка Апартамента");
        action().sendKeys(page.inputClientEmail, "test@test.ru");
        action().sendKeys(page.inputClientPhone, "5553332211");

        action().click(page.nextStep);

        try {
            action().waitBy(page.inputSnils);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент окна 2-го шага в окне бронирования не найден");
        }

        action().sendKeys(page.inputSnils, "55544433322");
        action().sendKeys(page.inputPasportSeriesNumber, "9200 776655");
        action().sendKeys(page.textareaWhoGiven, "выдан Администрацией Тринадцатого района города Париж");
        action().sendKeys(page.inputDateGiven, "12122000");
        action().sendKeys(page.inputCodeDepartament, "555-777");

        // JPG
        String jpg = System.getProperty("user.dir") + "\\files\\111.jpg";
        action().setClipboardData(jpg);
        action().click(page.passportPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        sleep(2000);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        try {
            Assert.assertTrue(action().findElement(By.xpath("//div[1][@ng-repeat='file in loadedFiles']/img"))
                    .getAttribute("src").contains(".jpeg"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Файл с расширением JPG не загрузился");
        }

        // PDF
        String pdf = System.getProperty("user.dir") + "\\files\\222.pdf";
        action().setClipboardData(pdf);
        action().click(page.passportPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        sleep(2000);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        try {
            Assert.assertTrue(action().findElement(By.xpath("//div[@ng-repeat='file in loadedFiles']/a"))
                    .getAttribute("alt").equals("pdf"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Файл с расширением JPG не загрузился");
        }

        // PNG
        String png = System.getProperty("user.dir") + "\\files\\333.png";
        action().setClipboardData(png);
        action().click(page.passportPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        sleep(2000);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        try {
            Assert.assertTrue(action().findElement(By.xpath("//div[3][@ng-repeat='file in loadedFiles']/img"))
                    .getAttribute("src").contains(".png"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Файл с расширением JPG не загрузился");
        }

        action().click(page.reservePopUp);

        try {
            action().waitBy(page.popUpElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент окна успешной отправки заявки не найден");
        }

        action().click(page.newReserve);

        try {
            action().waitBy(page.inputClientFirstName);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент окна успешной отправки заявки не найден");
        }

    }

    @Test(dependsOnMethods = "sendOrderFromSearchPage", description = "Проверка отображения заявки в админке, созданной на странице поиска", priority = 13)
    public void newOrderFromSearchPage() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        page.authorizationInAdminPanel();

        action().click(page.ordersSection);

        try {
            action().waitBy(page.orderNumber);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Заявки' не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.clientFullName).getText().contains("Тест Страница Поиска"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("ФИО клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.clientPhoneNumber).getText().contains("+7 111 222-33-44"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер телефона клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.apartmentType).getText().contains("2, 3, Ст"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Тип апартамента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.statusLabel).getText().contains("Новая"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заявки не 'Новая'");
        }

    }

    @Test(dependsOnMethods = "reserveApart", description = "Проверка отображения заявки в админке, созданной в карточке апартамента", priority = 9)
    public void newOrderFromApartmentCard() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        page.authorizationInAdminPanel();

        action().click(page.ordersSection);

        page.findOrderNumberInAdminPanel();

        try {
            Assert.assertTrue(action().findElement(page.clientFullName).getText().contains("Тест Карточка Апартамента"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("ФИО клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.clientPhoneNumber).getText().contains("+7 555 333-22-11"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер телефона клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.statusLabel).getText().contains("Новая"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заявки не 'Новая'");
        }

        action().click(page.orderNumber);

        try {
            action().waitBy(page.orderCardElement);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Элемент карточки 'Заявки' не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.cardStatusLabel).getText().contains("Новая"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заявки не 'Новая'");
        }

        try {
            Assert.assertTrue(action().findElement(page.cardClientFullName).getText().contains("Тест Карточка Апартамента"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("ФИО клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.cardClientPhoneNumber).getText().contains("+7 555 333-22-11"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер телефона клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.clientEmail).getText().contains("test@test.ru"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Электронная почта клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.passportData).getText().contains("9200 776655, выдан Администрацией Тринадцатого района города Париж 12.12.2000, 555-777"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Паспортные данные клиента не совпадают");
        }

        try {
            Assert.assertTrue(action().findElement(page.snils).getText().contains("555-444-333 22"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("СНИЛС клиента не совпадает");
        }

        action().click(page.documents);

        try {
            action().waitBy(By.xpath("//span[text()='pdf']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Файл в формате PDF не найден");
        }

        List<WebElement> images = driver.findElements(By.xpath("//div/img"));

        try {
            Assert.assertEquals(images.size(), 2);
        } catch (AssertionError ex){
            action().afterExceptionMessage("Количество картинок не совпадает");
        }

    }

}
