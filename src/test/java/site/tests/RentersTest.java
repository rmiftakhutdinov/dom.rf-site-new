package site.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.Test;
import site.BaseTest;
import site.Configuration;
import site.pageObjects.HomePage;
import static java.lang.Thread.sleep;

public class RentersTest extends BaseTest {

    @Test(description = "Свернуть и развернуть статьи ФАКа")
    public void hideAndShowFAQ() throws InterruptedException {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url + "for-renters");

        try {
            action().waitBy(page.rentersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Арендаторам' не найден");
        }

        action().click(page.parking);
        sleep(500);
        Assert.assertTrue(action().findElement(page.parkingElement).getText().contains("Для жителей Арендного дома «Лайнер»"));

        action().click(page.parking);
        sleep(500);
        Assert.assertFalse(action().findElement(page.parkingElement).isDisplayed());

        action().click(page.internet);
        sleep(500);
        Assert.assertTrue(action().findElement(page.internetElement).getText().contains("Во все апартаменты заведен"));

        action().click(page.internet);
        sleep(500);
        Assert.assertFalse(action().findElement(page.internetElement).isDisplayed());

    }

    @Test(description = "Открыть документы", priority = 1)
    public void openRentersDocuments() throws InterruptedException {

        HomePage page = new HomePage(driver);

        String handleBefore = driver.getWindowHandle();

        driver.get(Configuration.url + "for-renters");

        try {
            action().waitBy(page.rentersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Арендаторам' не найден");
        }

        action().click(page.documentRenters1);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc1 = "documents/Lajner_tipovoi_dogovor_bronirovanija_s_fiz_licom.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc1));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.documentRenters2);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc2 = "documents/Lajner_soglasie_na_obrabotky_pers_dannih.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.documentRenters3);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc3 = "documents/Lajner_dogovor_arendy.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc3));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.documentRenters4);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc4 = "documents/Dolgosrochnyj_dogovor_arendy_s_FL_redakcija_1.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc4));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.documentRenters5);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc5 = "documents/Lajner_dogovor_arendy_avto_fiz_lico.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc5));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.documentRenters6);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc6 = "documents/Prejskurant_na_dopolnitelnye_uslugi_10_05.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc6));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.documentRenters7);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc7 = "documents/living_rules.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc7));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

    }

}
