package site.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import site.BaseTest;
import site.Configuration;
import site.pageObjects.AdminPanelPage;
import site.pageObjects.HomePage;
import site.pageObjects.LinerLandingPage;
import site.pageObjects.SearchPage;
import java.util.List;
import static java.lang.Thread.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LinerLandingTest extends BaseTest {

    @Test(description = "Переход по ссылкам в плавающем меню страницы")
    public void headerLinksSwimMenu() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.logoSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент логотипа в плавающем меню не найден");
        }

        action().click(page.logoSwim);

        new HomePage(driver).findHomePageElement();

        driver.navigate().back();

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.findApartSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на страницу поиска в плавающем меню не найден");
        }

        action().click(page.findApartSwim);

        new SearchPage(driver).findSearchPageElement();

        driver.navigate().back();

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.rentersSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на страницу 'Арендаторам' в плавающем меню не найден");
        }

        action().click(page.rentersSwim);

        try {
            action().waitBy(new HomePage(driver).rentersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Арендаторам' не найден");
        }

        driver.navigate().back();

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.partnersSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на страницу 'Партнёрам' в плавающем меню не найден");
        }

        action().click(page.partnersSwim);

        try {
            action().waitBy(new HomePage(driver).partnersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Партнёрам' не найден");
        }

        driver.navigate().back();

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.investorsSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на страницу 'Инвесторам' в плавающем меню не найден");
        }

        action().click(page.investorsSwim);

        try {
            action().waitBy(new HomePage(driver).investorsPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Инвесторам' не найден");
        }

        driver.navigate().back();

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.massMediaSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на страницу 'СМИ о нас' в плавающем меню не найден");
        }

        action().click(page.massMediaSwim);

        try {
            action().waitBy(new HomePage(driver).massMediaPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'СМИ о нас' не найден");
        }

        driver.navigate().back();

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.cabinetSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на страницу ЛК в плавающем меню не найден");
        }

        action().click(page.cabinetSwim);

        try {
            action().waitBy(new HomePage(driver).authPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы авторизации в ЛК не найден");
        }

        action().click(new HomePage(driver).logoCabinet);

        new HomePage(driver).findHomePageElement();

        action().click(new HomePage(driver).liner);

        page.findLinerLandingPageElement();

        page.findFooterElement();

        action().scrollToElement(page.sendApplicationFooter);

        try {
            action().waitBy(page.changeToEnglishSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки смены языка в плавающем меню не найден");
        }

        action().click(page.changeToEnglishSwim);

        try {
            action().waitBy(page.changeToRussianSwim);
            action().waitBy(page.sendApplicationFooterInEnglish);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Перевод на английский язык не сработал");
        }

        action().scrollToElement(page.sendApplicationFooterInEnglish);

        try {
            action().waitBy(page.changeToRussianSwim);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки смены языка в плавающем меню не найден");
        }

        action().click(page.changeToRussianSwim);

        try {
            action().waitBy(page.changeToEnglishSwim);
            action().waitBy(page.sendApplicationFooter);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Перевод на русский язык не сработал");
        }

    }

    @Test(description = "Открыть и закрыть окно карты", priority = 1)
    public void openMap() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        String handleBefore = driver.getWindowHandle();

        try {
            action().waitBy(page.lookOnMap);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на карту не найден");
        }

        action().click(page.lookOnMap);

        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String coordinates = "55.789087, 37.538675";

        try {
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='clipboard__action-wrapper _inline']"))
                    .getText().contains(coordinates));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Координаты на карте не верны");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        try {
            action().waitBy(page.popUpPark);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpPark);

        try {
            action().waitBy(page.lookOnMapInPopUp);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на карту в модальном окне не найден");
        }

        action().click(page.lookOnMapInPopUp);

        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='clipboard__action-wrapper _inline']"))
                    .getText().contains(coordinates));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Координаты на карте не верны");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        page.closePopUp();

        try {
            action().waitBy(page.popUpShop);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpShop);

        try {
            action().waitBy(page.lookOnMapInPopUp);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на карту в модальном окне не найден");
        }

        action().click(page.lookOnMapInPopUp);

        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='clipboard__action-wrapper _inline']"))
                    .getText().contains(coordinates));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Координаты на карте не верны");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        page.closePopUp();

        try {
            action().waitBy(page.popUpTransport);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpTransport);

        try {
            action().waitBy(page.lookOnMapInPopUp);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на карту в модальном окне не найден");
        }

        action().click(page.lookOnMapInPopUp);

        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='clipboard__action-wrapper _inline']"))
                    .getText().contains(coordinates));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Координаты на карте не верны");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        page.closePopUp();

    }

    @Test(description = "Открыть поп-ап окна и прокликать их", priority = 2)
    public void popUpWindows() {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        try {
            action().waitBy(page.popUpPark);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpPark);

        try {
            action().waitBy(page.popUpParkElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.popUpShopElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.popUpShop);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpShop);

        try {
            action().waitBy(page.popUpShopElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.popUpParkElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.popUpSport);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpSport);

        try {
            action().waitBy(page.popUpSportElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.popUpShopElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.popUpTransport);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpTransport);

        try {
            action().waitBy(page.popUpTransportElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.picNoCarElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.popUpSafe);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpSafe);

        try {
            action().waitBy(page.popUpSafeElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.popUpParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.popUpParking);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.popUpParking);

        try {
            action().waitBy(page.popUpParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.picNoCarElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.picNoCar);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.picNoCar);

        try {
            action().waitBy(page.picNoCarElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.popUpParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.picParking);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.picParking);

        try {
            action().waitBy(page.popUpParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        try {
            action().waitBy(page.popUpInto);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на другое модальное окно не найден");
        }

        action().click(page.popUpInto);

        try {
            action().waitBy(page.picNoCarElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

        try {
            action().waitBy(page.picUK);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на модальное окно не найден");
        }

        action().click(page.picUK);

        try {
            action().waitBy(page.picUKElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна не найден");
        }

        page.closePopUp();

    }

    @Test(description = "Прокликать фотогалерею", priority = 3)
    public void galleryPhoto() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        try {
            action().waitBy(page.galleryHeader);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на галерею в хедере страницы не найден");
        }

        action().click(page.galleryHeader);

        try {
            action().waitBy(page.galleryHeader);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент открытой галереи в хедере страницы не найден");
        }

        String headerImage1 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControlInGallery);
        sleep(1000);

        String headerImage2 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControlInGallery);
        sleep(1000);

        String headerImage3 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.prevControlInGallery);
        sleep(1000);

        assertThat(headerImage1.equals(headerImage2) && headerImage2.equals(headerImage3) && headerImage1.equals(headerImage3), is(false));

        action().click(page.closeModalPhotos);

        try {
            action().waitBy(page.flatPlan);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на галерею со схемой апартаментов не найден");
        }

        action().click(page.flatPlan);

        try {
            action().waitBy(page.nextControlInGallery);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент открытой галереи со схемой апартаментов не найден");
        }

        String planImage1 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControlInGallery);
        sleep(1000);

        String planImage2 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControlInGallery);
        sleep(1000);

        String planImage3 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.prevControlInGallery);
        sleep(1000);

        assertThat(planImage1.equals(planImage2) && planImage2.equals(planImage3) && planImage1.equals(planImage3), is(false));

        action().click(page.closeModalPhotos);

        try {
            action().waitBy(page.apartGallery);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент ссылки на галерею с фотографиями апартаментов не найден");
        }

        action().click(page.apartGallery);

        try {
            action().waitBy(page.nextControlInGallery);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент открытой галереи с фотографиями апартаментов не найден");
        }

        String apartImage1 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControlInGallery);
        sleep(1000);

        String apartImage2 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.nextControlInGallery);
        sleep(1000);

        String apartImage3 = action().findElement(By.xpath("//div[@class='modal-photos ng-scope']//div[@class='tru-slider__viewport tru-slider__viewport--carousel ng-scope']//div[2]//div[2]")).getAttribute("style");

        action().click(page.prevControlInGallery);
        sleep(1000);

        assertThat(apartImage1.equals(apartImage2) && apartImage2.equals(apartImage3) && apartImage1.equals(apartImage3), is(false));

        action().click(page.closeModalPhotos);

    }

    @Test(description = "Скролл к форме отправки заявки", priority = 4)
    public void scrollToSendApplication() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        action().scrollToElement(page.flatPlan);

        action().click(page.sendApplicationInPlan);

        sleep(500);

        action().scrollToElement(page.apartGallery);

        action().click(page.sendApplicationAparts);

        sleep(500);

    }

    @Test(description = "Свернуть и развернуть блок 'Вопросы и ответы'", priority = 5)
    public void hideAndShowQuestionsAndAnswers() throws InterruptedException {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        action().click(page.questAndAnsw);
        sleep(500);

        try {
            Assert.assertTrue(action().findElement(page.textInQuestAndAnswArticle).getText().contains("Договор аренды заключается"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Вопрос не развернулся");
        }

        action().click(page.questAndAnsw);
        sleep(500);

        try {
            Assert.assertFalse(action().findElement(page.textInQuestAndAnswArticle).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Вопрос не свернулся");
        }

    }

    @Test(description = "Отправить заявку", priority = 6)
    public void sendApplication() {

        LinerLandingPage page = new LinerLandingPage(driver);

        driver.get(Configuration.url + "liner");

        page.findLinerLandingPageElement();

        action().sendKeys(page.inputName, "Тест Лендинг Лайнер");
        action().sendKeys(page.inputPhone, "6667778899");

        List<WebElement> labels = driver.findElements(By.xpath("//label/span"));
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            try {
                Assert.assertTrue(checkbox.isSelected());
            } catch (AssertionError ex){
                action().afterExceptionMessage("Отметка в чекбоксах не проставлена");
            }

        }

        try {
            action().waitBy(page.sendApplicationFooter);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки отправки заявки не найден");
        }

        action().click(page.sendApplicationFooter);

        try {
            action().waitBy(page.thanksForApplication);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент экрана успешной отправки заявки не найден");
        }

        try {
            action().waitBy(page.newApplication);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки отправки заявки не найден");
        }

        action().click(page.newApplication);

        try {
            action().waitBy(page.inputName);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент поля ввода имени пользователя не найден");
        }

    }

    @Test(dependsOnMethods = "sendApplication", description = "Проверка отображения заявки в админке, созданной на странице лендинга 'Лайнер'", priority = 7)
    public void newOrderFromLandingLiner() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        page.authorizationInAdminPanel();

        action().click(page.ordersSection);

        page.findOrderNumberInAdminPanel();

        try {
            Assert.assertTrue(action().findElement(page.clientFullName).getText().contains("Тест Лендинг Лайнер"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("ФИО клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.clientPhoneNumber).getText().contains("+7 666 777-88-99"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер телефона клиента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.apartmentType).getText().contains("2, 3, Ст"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Тип апартамента не совпадает");
        }

        try {
            Assert.assertTrue(action().findElement(page.statusLabel).getText().contains("Новая"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заявки не 'Новая'");
        }

    }

}
