package site.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.Test;
import site.BaseTest;
import site.Configuration;
import site.pageObjects.HomePage;
import static java.lang.Thread.sleep;

public class PartnersTest extends BaseTest {

    @Test(description = "Открыть документ 'Порядок и условия финансирования арендных проектов'")
    public void openPartnersDocument() throws InterruptedException {

        HomePage page = new HomePage(driver);
        String handleBefore = driver.getWindowHandle();

        driver.get(Configuration.url + "for-partners");

        try {
            action().waitBy(page.partnersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Партнерам' не найден");
        }

        action().click(page.documentPartners);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String doc = "documents/poryadok_i_usloviya_finansirovaniya.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(doc));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

    }

}
