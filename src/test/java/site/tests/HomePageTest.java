package site.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.Test;
import site.BaseTest;
import site.Configuration;
import site.pageObjects.HomePage;
import site.pageObjects.LinerLandingPage;
import static java.lang.Thread.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HomePageTest extends BaseTest {

    @Test(description = "Прокликать слайдер")
    protected void slider() throws InterruptedException {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        String image1 = action().findElement(By.xpath("//div[@class='front-page-line front-page-line__offer for-home-1']")).getAttribute("class");

        action().click(page.nextPage);
        sleep(500);

        String image2 = action().findElement(By.xpath("//div[@class='front-page-line front-page-line__offer for-home-2']")).getAttribute("class");

        action().click(page.nextPage);
        sleep(500);

        String image3 = action().findElement(By.xpath("//div[@class='front-page-line front-page-line__offer for-home-3']")).getAttribute("class");

        action().click(page.prevPage);
        sleep(500);
        assertThat(image1.equals(image2) && image2.equals(image3) && image1.equals(image3), is(false));

    }

    @Test(description = "Переход на лендинг 'Лайнер'", priority = 1)
    protected void goToLinerFromHomePage() {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        action().click(page.liner);

        new LinerLandingPage(driver).findLinerLandingPageElement();

    }

    @Test(description = "Переход на страницу поиска", priority = 2)
    protected void goToSearchPage() {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        action().click(page.toSearchPageLink);

        try {
            action().waitBy(page.searchPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Страница поиска не загрузилась");
        }

    }

    @Test(description = "Меню 'Все напрваления ДОМ.РФ'", priority = 3)
    protected void menuAllDirectionsDOMRF() {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        action().click(page.menuAllDirectionsAhml);

        try {
            action().waitBy(page.toSiteGroupLink);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Меню не развернулось");
        }

        action().click(page.toSiteGroupLink);

        try {
            action().waitBy(page.siteGroupPageElement);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Элемент сайта группы не найден");
        }

        driver.navigate().back();
        page.findHomePageElement();

        action().click(page.menuAllDirectionsAhml);

        try {
            action().waitBy(page.toSiteGroupLink);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Меню не развернулось");
        }

        action().click(page.menuAllDirectionsAhml);
        action().isElementNotPresent(page.toSiteGroupLink);

    }

    @Test(description = "Переход по ссылкам в хедере страницы", priority = 4)
    protected void headerLinks() {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        action().click(page.findApartHeader);

        try {
            action().waitBy(page.searchPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы поиска не найден");
        }

        action().click(page.logoHeader);
        page.findHomePageElement();

        action().click(page.rentersHeader);

        try {
            action().waitBy(page.rentersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Арендаторам' не найден");
        }

        driver.navigate().back();
        page.findHomePageElement();

        action().click(page.partnersHeader);

        try {
            action().waitBy(page.partnersPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Партнерам' не найден");
        }

        driver.navigate().back();
        page.findHomePageElement();

        action().click(page.investorsHeader);

        try {
            action().waitBy(page.investorsPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Инвесторам' не найден");
        }

        driver.navigate().back();
        page.findHomePageElement();

        action().click(page.massMediaHeader);

        try {
            action().waitBy(page.massMediaPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'СМИ о нас' не найден");
        }

        driver.navigate().back();
        page.findHomePageElement();

        action().click(page.cabinet);

        try {
            action().waitBy(page.authPageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы авторизации в ЛК не найден");
        }

        action().click(page.logoCabinet);
        page.findHomePageElement();

    }

    @Test(description = "Переход по ссылкам в футере страницы", priority = 5)
    protected void footerLinks() throws InterruptedException {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        String handleBefore = driver.getWindowHandle();

        action().click(page.doc1);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(page.document1));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.doc2);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(page.document2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.doc3);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(page.document3));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.facebook);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            action().waitBy(page.facebookElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы Facebook не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.instagram);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            action().waitBy(page.instagramElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы Instagram не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.vkontakte);
        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        try {
            action().waitBy(page.vkontakteElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы Вконтакте не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Смена языка", priority = 6)
    protected void changeLanguage() throws InterruptedException {

        HomePage page = new HomePage(driver);

        driver.get(Configuration.url);

        page.findHomePageElement();

        try {
            action().waitBy(page.changeToEnglish);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки переключения языка не найден");
        }

        action().click(page.changeToEnglish);

        try {
            action().waitBy(page.changeToRussian);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Перевод на английский язык не сработал");
        }

        sleep(500);

        action().click(page.changeToRussian);

        try {
            action().waitBy(page.changeToEnglish);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Перевод на русский язык не сработал");
        }

    }

}
