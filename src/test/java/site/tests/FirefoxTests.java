package site.tests;

import org.testng.annotations.*;
import site.BaseTest;
import site.Configuration;
import site.framework.DriverManager;
import site.pageObjects.SearchPage;

public class FirefoxTests extends BaseTest {

    @BeforeTest
    public void beforeTest(){

        driver = new DriverManager().getDriver("windows", "firefox");
        browser().maximize();
    }

    @AfterTest
    public void afterTest(){

            driver.close();
    }

    @Test(description = "Раскрыть текст описания апартамента по кнопке 'Подробнее'")
    public void showDescriptionOfApartment() {

        SearchPage page = new SearchPage(driver);

        driver.get(Configuration.url + "search");

        page.findSearchPageElement();

        page.goToApartment();

        action().click(page.moreInfo);

        action().isElementNotPresent(page.moreInfo);

    }

}
