package site.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    // HomePage
    public By homePageElement = By.xpath("//button[contains(text(), 'Подобрать жильё')]");

    public By nextPage = By.xpath("//div[@class='swiper-button-next tru-slider__handler']");

    public By prevPage = By.xpath("//div[@class='swiper-button-prev tru-slider__handler']");

    public By toSearchPageLink = By.xpath("//button[contains(text(),'Подобрать жильё')]");

    public By menuAllDirectionsAhml = By.xpath("//span[text()='Все направления ДОМ.РФ']");

    public By toSiteGroupLink = By.xpath("//a[contains(text(),'Перейти на сайт группы')]");

    public By siteGroupPageElement = By.xpath("//div[@class='top-line']//span[contains(text(),'Войти в личный кабинет')]");

    public By logoCabinet = By.xpath("//a[@class='auth-page-logo logo']");

    public By findApartHeader = By.xpath("//a[text()='Подобрать жильё']");

    public By rentersHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='renters']");

    public By partnersHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='partners']");

    public By investorsHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='investors']");

    public By massMediaHeader = By.xpath("//div[@class='std-pg-header-link']/a[@ui-sref='massmedia']");

    public By cabinet = By.xpath("//div[@class='std-pg-header__login-link-text ng-binding'][text()='Кабинет жильца']");

    public By changeToRussian = By.xpath("//a[@class='std-pg-header__lang-item std-pg-header__lang-item--ru ng-scope']");

    public By changeToEnglish = By.xpath("//a[@class='std-pg-header__lang-item std-pg-header__lang-item--en ng-scope']");

    public By logoHeader = By.xpath("//a[@class='s-ico s-ico-logo std-pg-header-logo ng-scope']");

    public By doc1 = By.xpath("//div[@class='std-pg-footer-box-link']/a[1]");

    public By doc2 = By.xpath("//div[@class='std-pg-footer-box-link']/a[2]");

    public By doc3 = By.xpath("//div[@class='std-pg-footer-box-link']/a[3]");

    public By facebook = By.xpath("//img[@alt='facebook']");

    public By facebookElement = By.xpath("//i/u[text()='Facebook']");

    public By instagram = By.xpath("//img[@alt='instagram']");

    public By instagramElement = By.xpath("//h1[@class='rhpdm'][text()='Аренда для жизни']");

    public By vkontakte = By.xpath("//img[@alt='vkontakte']");

    public By vkontakteElement = By.xpath("//div[@class='top_home_logo']");

    public By authPageElement = By.xpath("//h1[@class='auth-page-caption ng-binding'][text()='Вход в личный кабинет жильца']");


    // RentersPage
    public By rentersPageElement = By.xpath("//div[@class='partners-title ng-binding']");

    public By parking = By.xpath("//v-pane-header[@class='ng-binding ng-scope ng-isolate-scope'][text()='Предусмотрена ли парковка']");

    public By parkingElement = By.xpath("//div[@class='ng-binding ng-scope'][contains(text(),'Для жителей Арендного дома «Лайнер»')]");

    public By internet = By.xpath("//v-pane-header[@class='ng-binding ng-scope ng-isolate-scope'][text()='Есть ли Интернет в апартаментах']");

    public By internetElement = By.xpath("//*[text()='Есть ли Интернет в апартаментах']/following-sibling::*/div/div");

    public By documentRenters1 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/Lajner_tipovoi_dogovor_bronirovanija_s_fiz_licom.pdf')]");

    public By documentRenters2 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/Lajner_soglasie_na_obrabotky_pers_dannih.pdf')]");

    public By documentRenters3 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/Lajner_dogovor_arendy.pdf')]");

    public By documentRenters4 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/Dolgosrochnyj_dogovor_arendy_s_FL_redakcija_1.pdf')]");

    public By documentRenters5 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/Lajner_dogovor_arendy_avto_fiz_lico.pdf')]");

    public By documentRenters6 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/Prejskurant_na_dopolnitelnye_uslugi_10_05.pdf')]");

    public By documentRenters7 = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/living_rules.pdf')]");


    // PartnersPage
    public By documentPartners = By.xpath("//div[@class='partners-file_text']/a[contains(@href,'/documents/poryadok_i_usloviya_finansirovaniya.pdf')]");

    public By partnersPageElement = By.xpath("//div[@class='partners-title ng-binding']");


    // InvestorsPage
    public By linerInvestors = By.xpath("//a[@ui-sref='liner']");

    public By listOfCompanyInvestors = By.xpath("//a[@class='partners-file_link'][text()='здесь']");

    public By elementListOfCompanyInvestors = By.xpath("//h1[text()='О компании']");

    public By investorsPageElement = By.xpath("//div[@class='investors-title ng-binding']");


    // MassMediaPage
    public By massMediaPageElement = By.xpath("//div[@class='massmedia-title ng-binding']");


    // LinerPage
    public By liner = By.xpath("//div[@class='title ng-binding'][text()='Лайнер']");


    // SearchPage
    public By searchPageElement = By.xpath("//div[@class='aptm_search-find ng-binding'][contains(text(), 'Найдено')]");


    // Инициализация драйвера
    private WebDriver driver;
    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    // Поиск элемента главной страницы
    public void findHomePageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(homePageElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент Главной страницы не найден");
        }

    }

    // FooterDocuments
    public String document1 = "documents/Lajner_tipovoi_dogovor_bronirovanija_s_fiz_licom.pdf";
    public String document2 = "documents/Lajner_dogovor_arendy.pdf";
    public String document3 = "documents/Lajner_soglasie_na_obrabotky_pers_dannih.pdf";

}
