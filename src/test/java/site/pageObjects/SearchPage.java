package site.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {

    // Страница поиска
    public By roomStudio = By.xpath("//label[text()='Студия']");

    public By room2 = By.xpath("//label[text()='2']");

    public By room3 = By.xpath("//label[text()='3']");

    public By notEndFloor = By.xpath("//label[@for='check']");

    public By floorFrom = By.xpath("//input[@ng-model='filter.floor.from']");

    public By floorTo = By.xpath("//input[@ng-model='filter.floor.to']");

    public By floor15 = By.xpath("//div[@class='aptm_search-item_title ng-binding']");

    public By priceLow = By.xpath("//div[@class='handle low']");

    public By priceHigh = By.xpath("//div[@class='handle high']");

    public By sliderTrack = By.cssSelector("div.ng-isolate-scope.angular-range-slider");

    public By showFilter = By.xpath("//span[@class='aptm_search-filter_link ng-binding']");

    public By goToLiner = By.xpath("//a[@class='aptm_search-map-balloon_link']");

    public By page1 = By.xpath("//span[@class='page ng-binding ng-scope'][text()='1']");

    public By page1Active = By.xpath("//span[@class='page ng-binding ng-scope active'][contains(text(),'1')]");

    public By page2 = By.xpath("//span[@class='page ng-binding ng-scope'][text()='2']");

    public By page2Active = By.xpath("//span[@class='page ng-binding ng-scope active'][contains(text(),'2')]");

    public By pageNext = By.xpath("//span[@ng-click='goToPage(filter.page+1)'][contains(text(), 'Дальше')]");

    public By apartment = By.xpath("//div[@ng-if='!listLoading && aptmList.length']/a[1]");

    public By resetFilters = By.xpath("//span[text()='сбросьте фильтры']");

    public By apartmentPageElement = By.xpath("//div[@class='details-block']/div[contains(text(), 'Подробнее')]");

    public By noApartmentsElement = By.xpath("//h4[text()='Свободных апартаментов нет']");

    public By sendOrder = By.xpath("//button[text()='Отправить']");

    public By sendOrderElement = By.xpath("//div[contains(text(), 'спасибо за ваш интерес')]");

    public By spinner = By.xpath("//div[@class='loader']");

    public By inputName = By.xpath("//input[@name='name']");

    public By inputPhone = By.xpath("//input[@name='phone']");


    // Карточка апартамента
    public By liner = By.xpath("//a[@class='landing-link ng-binding'][contains(text(), 'Лайнер')]");

    public By gallery = By.xpath("//div[@class='tru-slider ng-scope']/div[3]/div[2]/div[2]");

    public By nextControl = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--next']");

    public By prevControl = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--prev']");

    public By closeModalPhotos = By.xpath("//div[@class='modal-photos__close']");

    public By miniPhotos = By.xpath("//div[@class='tru-slider__thumbs ng-scope']/div[3]/img[@class='tru-slider__thumb-image']");

    public By reserve = By.xpath("//div[@class='aptm-pg-top__view-order-button btn btn--green btn--l btn--lightSdO fx-row fx-center ng-binding']");

    public By inputClientFirstName = By.xpath("//input[@name='clientFirstName']");

    public By inputClientEmail = By.xpath("//input[@name='clientEmail']");

    public By inputClientPhone = By.xpath("//input[@name='clientPhone']");

    public By inputSnils = By.xpath("//input[@name='clientCode']");

    public By inputPasportSeriesNumber = By.xpath("//input[@name='passportSeriesNumber']");

    public By textareaWhoGiven = By.xpath("//textarea[@name='passportIssued']");

    public By inputDateGiven = By.xpath("//input[@name='passportIssuedAt']");

    public By inputCodeDepartament = By.xpath("//input[@name='passportCode']");

    public By nextStep = By.xpath("//button[contains(text(), 'Далее')]");

    public By reservePopUp = By.xpath("//button[contains(text(), 'Забронировать')]");

    public By popUpElement = By.xpath("//div[text()='Спасибо за заявку!']");

    public By newReserve = By.xpath("//div[@ng-click='clearReserv()']");

    public By moreInfo = By.xpath("//a[@class='description-link ng-binding ng-scope'][text()='Подробнее']");

    public By passportPhoto = By.xpath("//label[@class='reserv-popup__file-label s-ico s-ico-clip ng-binding']");


    // Инициализация драйвера
    private WebDriver driver;
    public SearchPage(WebDriver driver){
        this.driver = driver;
    }

    // Поиск элемента страницы поиска
    public void findSearchPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(new HomePage(driver).searchPageElement));
        } catch (TimeoutException ex){
            throw new RuntimeException("Элемент страницы поиска не найден");
        }

    }

    // Поиск элемента карточки апартамента
    public void findApartmentElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(new SearchPage(driver).apartmentPageElement));
        } catch (TimeoutException ex){
            throw new RuntimeException("Элемент карточки апартамента не найден");
        }

    }

    // Переход в карточку апартамента
    public void goToApartment(){

        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(apartment)).click();

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(apartmentPageElement));
        } catch (TimeoutException ex){
            throw new RuntimeException("Элемент карточки апартамента не найден");
        }

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(gallery));
        } catch (TimeoutException ex){
            throw new RuntimeException("Элемент галереи не найден");
        }

    }

}
