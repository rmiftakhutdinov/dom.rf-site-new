package site.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LinerLandingPage {

    public By landingLinerElement = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By galleryHeader = By.xpath("//div[@class='front-page-line__offer-box-title ng-binding'][text()='Арендное жилье в 10 минутах от центра']");

    public By prevControlInGallery = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--prev']");

    public By nextControlInGallery = By.xpath("//div[@class='tru-slider__handler tru-slider__handler--next']");

    public By closeModalPhotos = By.xpath("//div[@class='modal-photos__close']");

    public By lookOnMap = By.xpath("//div[@id='infrastructure']/button[@class='btn btn--green btn--l btn--lightSdO ng-binding'][text()='Посмотреть на карте']");

    public By popUpShop = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[1]");

    public By popUpShopElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][contains(text(),'Европе торгового центра')]");

    public By popUpPark = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[4]");

    public By popUpParkElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][text()='Места для прогулок']");

    public By popUpSport = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[6]");

    public By popUpSportElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][contains(text(),'Активный отдых и спорт')]");

    public By popUpTransport = By.xpath("//div[@class='front-page-line__environment-box-plan']/i[7]");

    public By popUpTransportElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][contains(text(),'10 минут до центра Москвы')]");

    public By popUpParking = By.xpath("//div[@class='front-page-line__comfort-pic']/i[3]");

    public By popUpParkingElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][text()='Подземный паркинг']");

    public By popUpSafe = By.xpath("//div[@class='front-page-line__comfort-pic']/i[4]");

    public By popUpSafeElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][contains(text(),'Безопасность 24 часа')]");

    public By popUpInto = By.xpath("//div[@class='front-page-popup__info']/div[4]");

    public By lookOnMapInPopUp = By.xpath("//div[@class='front-page-popup__info']/button");

    public By closePopUp = By.xpath("//div[@class='front-page-popup__close']");

    public By picNoCar = By.xpath("//div[@class='front-page-line__comfort-wrap']/div[1]");

    public By picNoCarElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][contains(text(),'Двор без машин')]");

    public By picParking = By.xpath("//div[@class='front-page-line__comfort-wrap']/div[2]");

    public By picUK = By.xpath("//div[@class='front-page-line__comfort-wrap']/div[3]");

    public By picUKElement = By.xpath("//div[@class='front-page-popup__info-title ng-binding'][contains(text(),'Професиональная управляющая компания')]");

    public By flatPlan = By.xpath("//div[@class='front-page-line__flat-box-wrap']/div[2]");

    public By sendApplicationInPlan = By.xpath("//div[@class='front-page-line__flat-box-content front-page-line-box-content']/button[text()='Оставить заявку']");

    public By apartGallery = By.xpath("//div[@class='front-page-line front-page-line front-page-line__photo-text']//div[@class='front-page-line__photo-box-content front-page-line-box-content']");

    public By sendApplicationAparts = By.xpath("//div[@class='front-page-line__photo-box-content front-page-line-box-content']/button[text()='Оставить заявку']");

    public By questAndAnsw = By.xpath("//div[@class='front-page-line__quest-box-wrap']//v-pane-header[@class='ng-binding ng-scope ng-isolate-scope'][contains(text(),'Какой минимальный срок аренды')]");

    public By textInQuestAndAnswArticle = By.xpath("//div[@class='front-page-line__quest-box-wrap']//v-accordion[1]//v-pane[3]//v-pane-content[1]//div[1]//div[1]");

    public By inputName = By.xpath("//input[@name='name']");

    public By inputPhone = By.xpath("//input[@name='phone']");

    public By sendApplicationFooter = By.xpath("//form[@name='orderForm']//button[contains(text(), 'Оставить заявку')]");

    public By thanksForApplication = By.xpath("//div[contains(text(), 'спасибо за заявку')]");

    public By newApplication = By.xpath("//div[contains(text(), 'новую заявку')]");

    public By logoSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']/a[1]");

    public By findApartSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Подобрать жильё']");

    public By rentersSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Арендаторам']");

    public By partnersSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Партнёрам']");

    public By investorsSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='Инвесторам']");

    public By massMediaSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//div[@class='std-pg-header-wrap']//a[@class='std-pg-header-link-item ng-binding ng-scope'][text()='СМИ о нас']");

    public By cabinetSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//a[@class='std-pg-header__login-link std-pg-header__login-link-mobile']/div[text()='Кабинет жильца']");

    public By changeToRussianSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//a[@class='std-pg-header__lang-item std-pg-header__lang-item--ru ng-scope']");

    public By changeToEnglishSwim = By.xpath("//div[@id='pgTop']/div[@class='ng-scope']//a[@class='std-pg-header__lang-item std-pg-header__lang-item--en ng-scope']");

    public By sendApplicationFooterInEnglish = By.xpath("//form[@name='orderForm']//button[contains(text(), 'Make request')]");


    // Инициализация драйвера
    public WebDriver driver;
    public LinerLandingPage(WebDriver driver){
        this.driver = driver;
    }

    // Поиск элемента страницы лендинга 'Лайнер'
    public void findLinerLandingPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(landingLinerElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы лендинга 'Лайнер' не найден");
        }

    }

    // Поиск элемента футера на странице лендинга 'Лайнер'
    public void findFooterElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(sendApplicationFooter));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент футера не найден");
        }

    }

    // Закрытие модального окна на странице лендинга 'Лайнер'
    public void closePopUp(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(closePopUp));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент кнопки закрытия модального окна не найден");
        }

        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(closePopUp)).click();

    }


}
