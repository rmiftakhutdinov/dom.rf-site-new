package site.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static java.lang.Thread.sleep;

public class AdminPanelPage {

    private By userName = By.xpath("//input[@id='inputEmail']");
    private By userPassword = By.xpath("//input[@id='inputPassword']");
    private By submit = By.xpath("//button");
    private By filterElement = By.xpath("//div[@class='search-input-wrapper multi-filter-wrapper ng-scope search-filter']");
    private By objectNumberElement = By.xpath("//tbody/tr[1]/td[2]/a");

    public By ordersSection = By.xpath("//a[text()='Заявки']");
    public By orderNumber = By.xpath("//tbody/tr[1]/td[2]/a");
    public By clientFullName = By.xpath("//tbody/tr/td[3]/a");
    public By clientPhoneNumber = By.xpath("//tbody/tr/td[3]/nobr");
    public By apartmentType = By.xpath("//tbody/tr/td[4]/span");
    public By statusLabel = By.xpath("//tr/td[7]/span[text()='Новая']");

    public By orderCardElement = By.xpath("//h1[contains(text(),'Заявка')]");
    public By cardStatusLabel = By.xpath("//span[text()='Новая']");
    public By cardClientFullName = By.xpath("//div[@class='panel-body']/div[1]/p[1]");
    public By cardClientPhoneNumber = By.xpath("//div[@class='panel-body']/div[1]/p[1]/a[1]");
    public By clientEmail = By.xpath("//div[@class='panel-body']/div[1]/p[1]/a[2]");
    public By passportData = By.xpath("//div[@class='panel-body']/div[1]/p[2]/span");
    public By snils = By.xpath("//div[@class='panel-body']/div[1]/p[3]/span");
    public By documents = By.xpath("//li[2][@role='presentation']/a");


    // Инициализация драйвера
    private WebDriver driver;
    public AdminPanelPage(WebDriver driver){
        this.driver = driver;
    }

    // Авторизация в Админке: user admin@example.com
    public void authorizationInAdminPanel() throws InterruptedException {

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(submit));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы авторизации не найден");
        }

        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys("admin@example.com");
        driver.findElement(userPassword).clear();
        driver.findElement(userPassword).sendKeys("spend-table-tttt-alcoa");

        sleep(1000);

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(filterElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент строки поиска в списке апартаментов не найден");
        }

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(objectNumberElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент апартамента в списке апартаментов не найден");
        }

    }

    public void findOrderNumberInAdminPanel(){

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(orderNumber));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент раздела 'Заявки' не найден");
        }

    }

}
